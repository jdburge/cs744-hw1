from pyspark import SparkContext
from pyspark import SparkConf
import sys

APP_NAME = 'page rank'
INPUT = sys.argv[1]
OUTPUT = sys.argv[2]
MASTER = sys.argv[3]
ITERATIONS = 10

def page_rank():
    # configuration
    conf = SparkConf().setAppName(APP_NAME)
    conf = conf.setMaster(MASTER)
    conf = conf.set("spark.executor.memory", "5g")
    conf = conf.set("spark.executor.cores", "1")
    sc = SparkContext(conf=conf)
    # core part of the script
    lines = sc.textFile(INPUT)
    pairs = lines.map(lambda s: (s.split("\t")[0], s.split("\t")[1] if len(s.split("\t")) > 1 else ""))
    ranks = pairs.reduceByKey(lambda x, y: 1.0)
    links = pairs.groupByKey().map(lambda s: (s[0], list(s[1])))

    for i in range(ITERATIONS):
        # contribs = links.join(ranks).flatMap(lambda url_links_rank: new_ranks(url_links_rank[1][0], url_links_rank[1][1]))
        contribs = links.join(ranks)
        contribs = contribs.flatMap(lambda url_links_rank: new_ranks(url_links_rank[1][0], url_links_rank[1][1]))

        ranks = contribs.reduceByKey(lambda x, y: x + y).mapValues(lambda sum: .15 + .85 * sum)
        
    ranks.saveAsTextFile(OUTPUT)

def page_rank_part():
    # configuration
    APP_NAME = 'page rank part'
    conf = SparkConf().setAppName(APP_NAME)
    conf = conf.setMaster(MASTER)
    conf = conf.set("spark.executor.memory", "5g")
    conf = conf.set("spark.executor.cores", "1")
    sc = SparkContext(conf=conf)
    # core part of the script
    lines = sc.textFile(INPUT, 700)
    pairs = lines.map(lambda s: (s.split("\t")[0], s.split("\t")[1] if len(s.split("\t")) > 1 else ""))
    ranks = pairs.reduceByKey(lambda x, y: 1.0)
    links = pairs.groupByKey().map(lambda s: (s[0], list(s[1])))

    for i in range(ITERATIONS):
        # contribs = links.join(ranks).flatMap(lambda url_links_rank: new_ranks(url_links_rank[1][0], url_links_rank[1][1]))
        contribs = links.join(ranks)
        contribs = contribs.flatMap(lambda url_links_rank: new_ranks(url_links_rank[1][0], url_links_rank[1][1]))

        ranks = contribs.reduceByKey(lambda x, y: x + y).mapValues(lambda sum: .15 + .85 * sum)
        
    ranks.saveAsTextFile(OUTPUT)

def page_rank_persist():
    # configuration
    APP_NAME = 'page rank persist'
    conf = SparkConf().setAppName(APP_NAME)
    conf = conf.setMaster(MASTER)
    conf = conf.set("spark.executor.memory", "5g")
    conf = conf.set("spark.executor.cores", "1")
    sc = SparkContext(conf=conf)
    # core part of the script
    lines = sc.textFile(INPUT)
    pairs = lines.map(lambda s: (s.split("\t")[0], s.split("\t")[1] if len(s.split("\t")) > 1 else ""))
    ranks = pairs.reduceByKey(lambda x, y: 1.0).persist()
    links = pairs.groupByKey().map(lambda s: (s[0], list(s[1])))

    for i in range(ITERATIONS):
        # contribs = links.join(ranks).flatMap(lambda url_links_rank: new_ranks(url_links_rank[1][0], url_links_rank[1][1]))
        contribs = links.join(ranks)
        contribs = contribs.flatMap(lambda url_links_rank: new_ranks(url_links_rank[1][0], url_links_rank[1][1]))

        ranks = contribs.reduceByKey(lambda x, y: x + y).mapValues(lambda sum: .15 + .85 * sum).persist()
        
    ranks.saveAsTextFile(OUTPUT)

def new_ranks(links, rank):
    size_links = len(links)
    try:
        rank = float(rank)
    except ValueError:
        rank = 1.0
    for link in links:
        yield (link, rank/size_links)

if __name__ == '__main__':
    page_rank()