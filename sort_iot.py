from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SQLContext
import sys

APP_NAME = 'sort iot'
MASTER = 'spark://c220g1-030613vm-1.wisc.cloudlab.us:7077'

INPUT = sys.argv[1]
OUTPUT = sys.argv[2]
MASTER = sys.argv[3]

def sort_iot():
    # Initialize Spark with appropriate master
    conf = SparkConf().setAppName(APP_NAME)
    conf = conf.setMaster(MASTER)
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)
    # Read csv data into DataFrame
    df = sqlContext.read.format("csv").option("header", "true").load(INPUT)
    # Perform SQL ordering operations
    df = df.orderBy("cca2", "timestamp")
    # Overwrite required for affecting hdfs directory
    df.write.mode('overwrite').csv(OUTPUT)
    
if __name__ == '__main__':
    sort_iot()
