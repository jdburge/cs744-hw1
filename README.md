# CS744-HW1

Part 1 ran locally with hellospark.py on a local spark cluster with 1 node (for memory purposes) using
https://github.com/mvillarrealb/docker-spark-cluster

Running Problem 1:

`/spark/bin/spark-submit /opt/spark-apps/hellospark.py`

`spark-2.2.0-bin-hadoop2.7/bin/spark-submit sort_iot.py hdfs://10.10.1.1:9000/export.csv hdfs://10.10.1.1:9000/iot_output.csv spark://c220g1-030613vm-1.wisc.cloudlab.us:7077`

a. Normal 55
b. persist 56
c. partition 700 1:30hr
d. break